from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

def create_super_user():
    User.objects.create_superuser('lokesh', 'lokeshsingh417@gmail.com', 'SeeaSingh417')

class Command(BaseCommand):
    def handle(self, **options):
        create_super_user()